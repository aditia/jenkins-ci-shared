def call(dockImgName, imgVersion) {
  // def app
  pipeline {
    agent any
    tools {
      go 'Go 1.15.2'
    }
    environment {
      // dockerImgName = "registry.hub.docker.com/superdit/${dockImgName}"
      dockerImgName = "registry.digitalocean.com/halalpedia/${dockImgName}"
      // dockerRegCreds = "DockerHub-Creds"
      dockerRegCreds = "DigitalOcean-Token-Creds"
      // dockerContainerUrl = "https://registry.hub.docker.com"
      dockerContainerUrl = "https://registry.digitalocean.com"

      dockerContainerImg = ""
    }
    stages {

      stage('Test Stage') {
        steps {
          echo "Test Stages"
          sh "go test"
        }
      }

      stage('Build Stage') {
        steps {
          echo "Build Stages"
          sh 'go build -o main .'
        }
      }

      stage('Code Analysis') {
        steps {
          echo "Code Analysis"
        }
      }

      stage('Docker Build Push') {
        steps {
          echo "Docker image name: ${dockerImgName}"
          sh "docker -v"

          // build image version / tag
          script {
            dockerContainerImg = docker.build dockerImgName + ":" + imgVersion
          }

          script {
            docker.withRegistry( dockerContainerUrl, dockerRegCreds ) {
              dockerContainerImg.push()
              dockerContainerImg.push("latest")
            }
          }
          
          echo "Removing docker images"
          sh "docker rmi $dockerImgName:$imgVersion"
          sh "docker rmi $dockerImgName:latest"
          sh "docker image prune -f --filter label=stage=builder"
          
          // app = docker.build("registry.digitalocean.com/halalpedia/mygoapp")
          // sh "docker login -u '${env.DO_USER}' --password-stdin '${env.DO_PASS}' ${env.DO_CONTREG_URL}"
          // echo "${env.DO_PASS}"
          // sh "docker login -u ${env.DO_USER} --password-stdin ${env.DO_PASS} ${env.DO_CONTREG_URL}"
          // sh "docker build -t goapptest${BUILD_NUMBER} ."
          // sh "docker tag goapptest${BUILD_NUMBER} superdit/pipeline_build_test:${BUILD_NUMBER}.0"
          // sh "docker push superdit/pipeline_build_test:${BUILD_NUMBER}.0"
        }
      }
      // end stage 
    }
    // end stages
  }
}
