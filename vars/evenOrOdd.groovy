def call(int buildNumber) {
  if (buildNumber % 2 == 0) {
    pipeline {
      agent any
      tools {
        go 'Go 1.15.2'
      }
      stages {
        stage('Even Stage') {
          steps {
            echo "The build number is even"
            sh 'docker version'
            sh 'go version'
          }
        }
      }
    }
  } else {
    pipeline {
      agent any
      tools {
        go 'Go 1.15.2'
      }
      stages {
        stage('Odd Stage') {
          steps {
            echo "The build number is odd"
            sh 'docker version'
            sh 'go version'
          }
        }
      }
    }
  }
}